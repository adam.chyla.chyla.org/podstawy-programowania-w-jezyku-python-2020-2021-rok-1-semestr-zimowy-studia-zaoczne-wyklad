#!/usr/bin/env python3
# shebang 


# TDD --> test driven development
#
# 1. piszemy test (assert)
# 2. uruchamiamy testy (widzimy, ze nie dzialaja)
# 3. piszemy implementacje (kod funkcji)
# 4. uruchamiamy testy (widzimy, ze dzialaja)
# 5. refactor (ulepszamy nasz kod) - zmiana kodu + uruchomienie testu
# 6. wracamy do 1


# co dalej?
# 1. refactor dla fibonacci
# 2. osobna funkcja dla:
#    assert 0 in cache
#    assert cache[0] == 0


def fibonacci(n, cache):
    if cache:
        if n in cache:
            return cache[n]

    if n <= 1:
        if cache is not None:
            cache[n] = n
        return n
    else:
        wynik = fibonacci(n-2, cache) + fibonacci(n-1, cache)
        if cache is not None:
            cache[n] = wynik
        return wynik


def sprawdz_fibonacci(n, oczekiwany_wynik, cache=None):
    wynik = fibonacci(n, cache)
    assert wynik == oczekiwany_wynik, ("dla n: " + str(n)
                                           + " funkcja powinna zwracac: "
                                           + str(oczekiwany_wynik)
                                           + ", zwrocila: "
                                           + str(wynik))


def testy_fibonacci():
    sprawdz_fibonacci(n=0, oczekiwany_wynik=0)
    sprawdz_fibonacci(n=1, oczekiwany_wynik=1)
    sprawdz_fibonacci(n=2, oczekiwany_wynik=1)
    sprawdz_fibonacci(n=3, oczekiwany_wynik=2)
    sprawdz_fibonacci(n=4, oczekiwany_wynik=3)
    sprawdz_fibonacci(n=10, oczekiwany_wynik=55)

    cache = {}
    sprawdz_fibonacci(n=0, oczekiwany_wynik=0, cache=cache)
    assert 0 in cache
    assert cache[0] == 0

    cache = {}
    sprawdz_fibonacci(n=1, oczekiwany_wynik=1, cache=cache)
    assert 1 in cache
    assert cache[1] == 1

    cache = {}
    sprawdz_fibonacci(n=2, oczekiwany_wynik=1, cache=cache)
    assert 2 in cache
    assert cache[2] == 1


    cache = {}
    sprawdz_fibonacci(n=3, oczekiwany_wynik=2, cache=cache)
    assert 2 in cache
    assert cache[2] == 1
    assert 3 in cache
    assert cache[3] == 2

    cache = {}
    sprawdz_fibonacci(n=4, oczekiwany_wynik=3, cache=cache)
    assert 2 in cache
    assert cache[2] == 1
    assert 3 in cache
    assert cache[3] == 2
    assert 4 in cache
    assert cache[4] == 3

    cache = {}
    sprawdz_fibonacci(n=10, oczekiwany_wynik=55, cache=cache)

    # czy cache jest uzywany?
    cache = { 4: 99 }
    sprawdz_fibonacci(n=4, oczekiwany_wynik=99, cache=cache)


testy_fibonacci()


# cache w pliku:
# numer_wyrazu:wartosc
# numer_wyrazu:wartosc
# numer_wyrazu:wartosc

def wczytaj_cache_z_pliku(cache, nazwa_pliku_cache):
    with open(nazwa_pliku_cache, "a") as cache_file:
        pass

    with open(nazwa_pliku_cache, "r") as cache_file:
        # for linia in cache_file:
        # readline() -- while
        # readlines() --> ['linia 1', 'linia 2', 'linia 3', ... ]
        for linia in cache_file:
            w = linia.split(':')
            klucz = int(w[0])
            wartosc = int(w[1])
            cache[klucz] = wartosc


def zapisz_cache_do_pliku(cache, nazwa_pliku_cache):
    # w - zapisywanie
    # r - odczytywanie
    # a - dopisywanie
    # r+ - zapis/odczyt
    with open(nazwa_pliku_cache, "w") as cache_file:
        for numer_wyrazu, wartosc in cache.items():
            linia_w_pliku = f"{numer_wyrazu}:{wartosc}\n"
            cache_file.write(linia_w_pliku)


def main():
    cache = {}
    nazwa_pliku_cache = "cache.txt"
    
    wczytaj_cache_z_pliku(cache, nazwa_pliku_cache)
    
    numer_wyrazu = int(input("Podaj numer wyrazu do obliczenia: "))
    wynik = fibonacci(numer_wyrazu, cache)
    print("Wynik:", wynik)

    zapisz_cache_do_pliku(cache, nazwa_pliku_cache)


main()
