# 1. zapytaj o x
# 2. zapytaj o y
# 3. zapytaj o operacje +,-,*,/
# 4. wypisz wynik

#
# TDD --> test driven development
#
# 1. piszemy test (assert)
# 2. uruchamiamy testy (widzimy, ze nie dzialaja)
# 3. piszemy implementacje (kod funkcji)
# 4. uruchamiamy testy (widzimy, ze dzialaja)
# 5. refactor (ulepszamy nasz kod)
# 6. wracamy do 1

def czy_liczba(wartosc):
    if wartosc == "":
        return False
    
    if wartosc[0] in '-+0123456789':
        for znak in wartosc[1:]:
            if znak not in '0123456789':
                return False
    else:
        return False

    return True

assert czy_liczba("") is False
assert czy_liczba("1") is True
assert czy_liczba("123") is True
assert czy_liczba("a") is False
assert czy_liczba("abcd") is False
assert czy_liczba("1bcd") is False
assert czy_liczba("1bc3") is False
assert czy_liczba("-1") is True
assert czy_liczba("-10001") is True
assert czy_liczba("123-135") is False
assert czy_liczba("+1") is True
assert czy_liczba("+14873") is True
assert czy_liczba("132+123") is False


# int, float
# z = 1.5
# zc = int(z)

# True, False
czy_wyjsc = False

while not czy_wyjsc:
    czy_zatrzymac = False
    
    x = input("Podaj x: ")
    if not czy_liczba(x):
        czy_zatrzymac = True

    y = input("Podaj y: ")
    if not czy_liczba(y):
        czy_zatrzymac = True
        
    if czy_zatrzymac:
        print("Podane wartosci nie sa liczba")
        continue

    x = int(x)
    y = int(y)

    operacja = input("Wybierz operacje (+,-,*,/,exit): ")

    if operacja == "+":
        wynik = x + y
        print("Wynik:", wynik)
    elif operacja == "-":
        wynik = x - y
        print("Wynik:", wynik)
    elif operacja == "*":
        wynik = x * y
        print("Wynik:", wynik)
    elif operacja == "/":
        if y == 0:
            print("Nie dziele przez zero")
        else:
            wynik = x / y
            print("Wynik:", wynik)
    elif operacja == "exit":
        czy_wyjsc = True
    else:
        print("Wybrano zla operacje")

        