# 1. zapytaj o x
# 2. zapytaj o y
# 3. zapytaj o operacje +,-,*,/
# 4. wypisz wynik

# int, float
# z = 1.5
# zc = int(z)

# True, False
czy_wyjsc = False

while not czy_wyjsc:
	x = input("Podaj x: ")  # -> str
	
	czy_zatrzymac = False
	for znak in x:
		if znak not in "0123456789":
			print("Blad, x nie jest liczba")
			czy_zatrzymac = True
			break

	if czy_zatrzymac:
		continue

	x = int(x)

	y = int(input("Podaj y: "))  # -> str
	
	operacja = input("Wybierz operacje (+,-,*,/,exit): ")

	if operacja == "+":
		wynik = x + y
		print("Wynik:", wynik)
	elif operacja == "-":
		wynik = x - y
		print("Wynik:", wynik)
	elif operacja == "*":
		wynik = x * y
		print("Wynik:", wynik)
	elif operacja == "/":
		if y == 0:
			print("Nie dziele przez zero")
		else:
			wynik = x / y
			print("Wynik:", wynik)
	elif operacja == "exit":
		czy_wyjsc = True
	else:
		print("Wybrano zla operacje")

		