#!/usr/bin/env python3
# shebang 

from matematyka import fibonacci


def main():
    cache = {}
    numer_wyrazu = int(input("Podaj numer wyrazu do obliczenia: "))
    
    wynik = fibonacci(numer_wyrazu, cache)
    
    # 1
    # napis = "Wynik:" + str(wynik)
    
    # opcja % - stary sposob
    # napis = "Wynik: %d" % wynik

    # opcja format
    # napis = "Wynik: {}".format(wynik)
    
    # python 3.6, f-strings
    napis = f"Wynik: {{{wynik}}}"

    # ...

    # na ekranie Wynik: {3}
    
    print(napis)


if __name__ == "__main__":
    main()
