#!/usr/bin/env python3
# shebang 

import matematyka


def main():
    cache = {}
    numer_wyrazu = int(input("Podaj numer wyrazu do obliczenia: "))
    
    wynik = matematyka.fibonacci(numer_wyrazu, cache)
    
    print("Wynik:", wynik)

main()
