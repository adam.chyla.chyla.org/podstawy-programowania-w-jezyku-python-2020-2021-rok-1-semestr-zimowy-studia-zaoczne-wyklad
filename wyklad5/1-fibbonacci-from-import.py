#!/usr/bin/env python3
# shebang 

from matematyka import fibonacci


def main():
    cache = {}
    numer_wyrazu = int(input("Podaj numer wyrazu do obliczenia: "))
    
    wynik = fibonacci(numer_wyrazu, cache)
    
    print("Wynik:", wynik)

main()
