#!/usr/bin/env python3
# shebang 

from matematyka import fibonacci as fib


def main():
    cache = {}
    numer_wyrazu = int(input("Podaj numer wyrazu do obliczenia: "))
    
    wynik = fib(numer_wyrazu, cache)
    
    print("Wynik:", wynik)

main()
