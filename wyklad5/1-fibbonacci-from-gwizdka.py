#!/usr/bin/env python3
# shebang 

# nie zaleca sie uzywac
from matematyka import * # fibonacci
from math import *


def main():
    cache = {}
    numer_wyrazu = int(input("Podaj numer wyrazu do obliczenia: "))
    
    wynik = fibonacci(numer_wyrazu, cache)
    
    print("Wynik:", wynik)

main()
