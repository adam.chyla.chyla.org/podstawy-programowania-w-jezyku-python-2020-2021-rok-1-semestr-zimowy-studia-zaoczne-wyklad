# zapisanie listy pakietow do pliku
# pip freeze > requirements.txt


def download_webpage_source(url):
    import urllib.request

    request = urllib.request.Request(url,
                                     headers = {
                                         'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134"
                                     })

    text = urllib.request.urlopen(request).read()

    return text.decode()


def parse(html):
    from bs4 import BeautifulSoup

    soup = BeautifulSoup(html, 'html.parser')
    blok_13 = soup.find(id="blok_13")
    headers = blok_13.find_all("h3")

    # list comprehension
    return [h.text.strip() for h in headers]

    # odpowiednik
#    tmp = []
#    for h in headers:
#        tmp.append(h.text.strip())
#    return tmp


def main():
    import urllib

    try:
        # 1. pobieranie zrodla strony
        html = download_webpage_source("https://prezydent.pl/")

        # 2. etap parsowania
        headers = parse(html)

        print("Naglowki:")
        for text in headers:
            print(text)
    except urllib.error.URLError as e:
        print("Problem z pobraniem strony:", e)
    except Exception as e:
        print("Blad ogolny:", e)


if __name__ == '__main__':
    main()
